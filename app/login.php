<?php
include_once '../config/database.php';
require "../vendor/autoload.php";
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
// $uri = explode( '/', $uri );
// if ($uri[3] !== 'login') {
//     header("HTTP/1.1 404 Not Found");
//     exit();
// }

$username = '';
$password = '';

$databaseService = new DatabaseService();
$conn = $databaseService->getConnection();

if(isset($_POST['data']))
{
    //for form-data
    $obj  = $_POST['data']; 
    $data = json_decode($obj);
}
else
{
    //for raw data
    $data = json_decode(file_get_contents("php://input"));
}
//for form-data
// $obj  = $_POST['data']; 
// $data = json_decode($obj);
//for raw data
// $data = json_decode(file_get_contents("php://input"));
// var_dump($data);exit();
$username = $data->username;
$password = $data->password;
$table_name = 'tbl_user';

$query = "SELECT User_ID,User_Name,user_fName,user_lname,User_Pass,user_permission FROM " . $table_name . " WHERE User_Name = :name LIMIT 1";

$stmt = $conn->prepare( $query );
$stmt->bindParam(':name', $username);
$stmt->execute();
$num = $stmt->rowCount();
// echo $num;exit();
if($num > 0){
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    // var_dump($row);
    $id = $row['User_ID'];
    $firstname = $row['user_fName'];
    $lastname = $row['user_lname'];
    $password2 = $row['User_Pass'];
    $role = $row['user_permission'];
    if(password_verify($password, $password2))
    {
        $secret_key = "YOUR_SECRET_KEY";
        $issuer_claim = "THE_ISSUER"; // this can be the servername
        $audience_claim = "THE_AUDIENCE";
        $issuedat_claim = time(); // issued at
        $notbefore_claim = $issuedat_claim + 10; //not before in seconds
        $expire_claim = $issuedat_claim + 60; // expire time in seconds
        $token = array(
            "iss" => $issuer_claim,
            "aud" => $audience_claim,
            "iat" => $issuedat_claim,
            "nbf" => $notbefore_claim,
            "exp" => $expire_claim,
            "data" => array(
                "id" => $id,
                "firstname" => $firstname,
                "lastname" => $lastname,
                "username" => $username,
                "role" => $role,
        ));

        http_response_code(200);

        $jwt = JWT::encode($token, $secret_key);
        echo json_encode(
            array(
                "status" => "1",
                "message" => "Successful login.",
                "jwt" => $jwt,
                "username" => $username,
                "expireAt" => $expire_claim,
                "pic" => 'http://api.albatrossthai.com/images/abt.jpg'
            ));
    }
    else
    {

        http_response_code(401);
        echo json_encode(array("status" => "0","message" => "Login failed.", "password" => $password));
    }
}
?>